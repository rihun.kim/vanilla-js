//
const pad = document.querySelector("#pad");
const record = document.querySelector("#record");
const panel = document.querySelector("#panel");

//
let startTime;
let timer;
let records = [];

pad.addEventListener("click", function () {
  if (pad.classList.contains("waiting")) {
    // red 준비되시면 클릭해주세요.
    pad.classList.remove("waiting");
    pad.classList.add("ready");
    pad.textContent = "초록색이 되면 클릭해주세요.";
    timer = setTimeout(function () {
      startTime = new Date();
      pad.click();
    }, 1000 * Math.random() + 2000);
  } else if (pad.classList.contains("ready")) {
    // orange 초록색이 되면 클릭해주세요.
    // 컴퓨터가 자동으로 넘기는 구간
    if (startTime) {
      // 정상 클릭
      pad.classList.remove("ready");
      pad.classList.add("testing");
      pad.textContent = "클릭!!";
      panel.textContent = "";
    } else {
      // 부정클릭
      clearTimeout(timer);
      startTime = null;
      panel.textContent = "초록색되면 클릭해라.";
      pad.classList.remove("ready");
      pad.classList.add("waiting");
      pad.textContent = "준비되시면 클릭해주세요.";
    }
  } else {
    // green 클릭
    if (startTime) {
      let endTime = new Date();
      const value = endTime - startTime;
      records.push(value);
      records.sort((a, b) => a - b);
      let temp = records.map((record) => record + "ms").join(" , ");
      record.textContent = temp;
      startTime = null;
    }

    pad.classList.remove("testing");
    pad.classList.add("waiting");
    pad.textContent = "준비되시면 클릭해주세요.";
  }
});
