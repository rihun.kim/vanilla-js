//
const table = document.querySelector("#table");
const score = document.querySelector("#score");

//
let data = [];
let dragStart = false;
let draging = false;
let startPoint;
let endPoint;

//
init();
createRandom();

//
function init() {
  const fragment = document.createDocumentFragment();

  [1, 2, 3, 4].forEach(function () {
    data.push([0, 0, 0, 0]);
    let tr = document.createElement("tr");

    //
    [1, 2, 3, 4].forEach(function () {
      const td = document.createElement("td");
      tr.appendChild(td);
    });

    fragment.appendChild(tr);
  });

  table.appendChild(fragment);
}

function createRandom() {
  let temp = [];

  data.forEach(function (rowData, i) {
    rowData.forEach(function (colData, j) {
      if (!colData) temp.push([i, j]);
    });
  });

  if (temp.length === 0) {
    alert("GAME OVER" + score.textContent);
    table.innerHTML = "";
    init();
  } else {
    const randomBox = temp[Math.floor(Math.random() * temp.length)];
    data[randomBox[0]][randomBox[1]] = 2;
    paint();
  }
}

function paint() {
  data.forEach(function (rowData, i) {
    rowData.forEach(function (colData, j) {
      if (colData > 0) {
        table.children[i].children[j].textContent = colData;
      } else {
        table.children[i].children[j].textContent = "";
      }
    });
  });
}

//
window.addEventListener("mousedown", function (event) {
  dragStart = true;
  startPoint = [event.clientX, event.clientY];
});

window.addEventListener("mousemove", function (event) {
  if (dragStart) draging = true;
});

window.addEventListener("mouseup", function (event) {
  endPoint = [event.clientX, event.clientY];

  let way;
  if (draging) {
    let diffX = endPoint[0] - startPoint[0];
    let diffY = endPoint[1] - startPoint[1];

    if (diffX < 0 && Math.abs(diffX) / Math.abs(diffY) > 1) {
      way = "left";
    } else if (diffX > 0 && Math.abs(diffX) / Math.abs(diffY) > 1) {
      way = "right";
    } else if (diffY < 0 && Math.abs(diffX) / Math.abs(diffY) < 1) {
      way = "up";
    } else {
      way = "down";
    }
  }

  dragStart = false;
  draging = false;

  let newData = [[], [], [], []];
  switch (way) {
    case "left":
      data.forEach(function (rowData, i) {
        rowData.forEach(function (colData, j) {
          if (colData) {
            // colData = 2;
            // console.log("row, col ", i, j);
            // console.log("시작전", JSON.parse(JSON.stringify(newData)));

            if (newData[i][newData[i].length - 1] && newData[i][newData[i].length - 1] === colData) {
              newData[i][newData[i].length - 1] *= 2;
              score.textContent = +score.textContent + newData[i][newData[i].length - 1];
            } else {
              newData[i].push(colData);
            }
          }
        });
      });

      for (let i = 0; i < 4; ++i) {
        for (let j = 0; j < 4; ++j) {
          data[i][j] = newData[i][j] || 0;
        }
      }
      break;
    case "right":
      data.forEach(function (rowData, i) {
        rowData.forEach(function (colData, j) {
          if (colData) {
            if (newData[i][0] && newData[i][0] === colData) {
              newData[i][0] *= 2;
              score.textContent = +score.textContent + newData[i][0];
            } else newData[i].unshift(colData);
          }
        });
      });

      for (let i = 0; i < 4; ++i) {
        for (let j = 0; j < 4; ++j) {
          data[i][3 - j] = newData[i][j] || 0;
        }
      }
      break;
    case "up":
      data.forEach(function (rowData, i) {
        rowData.forEach(function (colData, j) {
          if (colData) {
            if (newData[j][newData[j].length - 1] && newData[j][newData[j].length - 1] === colData) {
              newData[j][newData[j].length - 1] *= 2;
              score.textContent = +score.textContent + newData[i][0] + newData[j][newData[j].length - 1];
            } else newData[j].push(colData);
          }
        });
      });

      // refreshing
      [1, 2, 3, 4].forEach(function (colData, i) {
        [1, 2, 3, 4].forEach(function (rowData, j) {
          data[j][i] = newData[i][j] || 0;
        });
      });

      break;
    case "down":
      data.forEach(function (rowData, i) {
        rowData.forEach(function (colData, j) {
          if (colData) {
            if (newData[j][0] && newData[j][0] === colData) {
              newData[j][0] *= 2;
              score.textContent = +score.textContent + newData[i][0] + newData[j][0];
            } else newData[j].unshift(colData);
          }
        });
      });

      // refreshing
      [1, 2, 3, 4].forEach(function (colData, i) {
        [1, 2, 3, 4].forEach(function (rowData, j) {
          data[3 - j][i] = newData[i][j] || 0;
        });
      });

      break;
  }

  paint();
  createRandom();
});
