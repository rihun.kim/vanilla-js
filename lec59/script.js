//
// 시간이 표시되는 구역
// 남은 시간
const timerDisplay = document.querySelector(".display__time-left");
// 돌아올 시간
const endTime = document.querySelector(".display__end-time");
// 상단 버튼들
const buttons = document.querySelectorAll("[data-time]");

//
let countdown;

//
// 타이머 가동
function timer(seconds) {
  // 이전 카운트 다운 지움
  clearInterval(countdown);

  const now = Date.now(); // 현재시간
  const then = now + seconds * 1000; // 미래시간

  // 그릴 것.
  displayTimeLeft(seconds);
  displayEndTime(then);

  // 인터벌 설정
  countdown = setInterval(() => {
    const secondsLeft = Math.round((then - Date.now()) / 1000);

    if (secondsLeft < 0) {
      clearInterval(countdown);
      return;
    }

    // 그리기
    displayTimeLeft(secondsLeft);
  }, 1000);
}

// 남은 시간 그리기
function displayTimeLeft(seconds) {
  const minutes = Math.floor(seconds / 60);
  const remainderSeconds = seconds % 60;
  const display = `${minutes}:${remainderSeconds < 10 ? "0" : ""}${remainderSeconds}`;

  document.title = display;
  timerDisplay.textContent = display;
}

// 끝나는 시간 그리기
function displayEndTime(timestamp) {
  const end = new Date(timestamp);
  const hour = end.getHours();
  const adjustedHour = hour > 12 ? hour - 12 : hour;
  const minutes = end.getMinutes();
  endTime.textContent = `Be Back At ${adjustedHour}:${minutes < 10 ? "0" : ""}${minutes}`;
}

function startTimer() {
  const seconds = parseInt(this.dataset.time);
  timer(seconds);
}

//
buttons.forEach((button) => button.addEventListener("click", startTimer));

document.customForm.addEventListener("submit", function (e) {
  e.preventDefault();
  const mins = this.minutes.value;
  timer(mins * 60);
  this.reset(); // input 기입 내용 지우기
});
