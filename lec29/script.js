//
const rows = [];
const boxs = [];
let turn = "X";

//
const table = document.createElement("table");
for (let i = 0; i < 3; ++i) {
  const tr = document.createElement("tr");
  rows.push(tr);
  boxs.push([]);
  for (let j = 0; j < 3; ++j) {
    const td = document.createElement("td");
    td.addEventListener("click", callback);
    tr.appendChild(td);
    boxs[i].push(td);
  }
  table.appendChild(tr);
}
document.body.appendChild(table);

//
function callback(e) {
  let rowNum = rows.indexOf(e.target.parentNode);
  let colNum = boxs[rowNum].indexOf(e.target);
  const prevTurn = turn;

  if (boxs[rowNum][colNum].textContent == "") {
    boxs[rowNum][colNum].textContent = turn;
    if (turn == "X") turn = "O";
    else turn = "X";
  }

  if (checkGameOver(rowNum, colNum, prevTurn)) {
    const res = document.createElement("div");
    res.textContent = "Winner is " + prevTurn;
    document.body.appendChild(res);
  }
}

function checkGameOver(rowNum, colNum, turn) {
  // 가로줄 검사
  let res = true;
  for (let i = 0; i < 3; ++i) {
    if (boxs[rowNum][i].textContent != turn) {
      res = false;
      break;
    }
  }
  if (res) return true;

  // 세로줄 검사
  res = true;
  for (let i = 0; i < 3; ++i) {
    if (boxs[i][colNum].textContent != turn) {
      res = false;
      break;
    }
  }
  if (res) return true;

  // 대각선 검사
  res = true;
  for (let i = 0; i < 3; ++i) {
    if (boxs[i][i].textContent != turn) {
      res = false;
      break;
    }
  }
  if (res) return true;

  res = true;
  for (let i = 0; i < 3; ++i) {
    if (boxs[i][2 - i].textContent != turn) {
      res = false;
      break;
    }
  }
  if (res) return true;

  return false;
}
