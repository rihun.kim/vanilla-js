function loadImages() {
  const req = new XMLHttpRequest();
  req.open("GET", "./images.json");
  req.onreadystatechange = function () {
    if (this.readyState == 4) {
      const images = JSON.parse(this.response);
      for (let imageUrl of images) {
        const divElement = document.createElement("div");
        divElement.setAttribute("class", "image");

        divElement.addEventListener("click", function () {
          this.classList.toggle("image-selected");
        });
        divElement.addEventListener("mouseover", function () {
          let element = this;
          this.timerId = setTimeout(function () {
            element.classList.add("image-magnified");
          }, 1000);
        });
        divElement.addEventListener("mouseout", function () {
          clearTimeout(this.timerId);
          this.classList.remove("image-magnified");
        });

        const imgElement = document.createElement("img");
        imgElement.src = imageUrl;

        divElement.appendChild(imgElement);
        document.body.appendChild(divElement);
      }
    }
  };
  req.send();
}

function selectAll(btn) {
  const images = document.getElementsByClassName("image");
  for (let image of images) {
    if (btn.value == "Select ALL") image.classList.add("image-selected");
    else image.classList.remove("image-selected");
  }

  if (btn.value == "Select ALL") btn.value = "Unselect ALL";
  else btn.value = "Select ALL";
}

function slideShow(btn) {
  let images = document.getElementsByClassName("image");

  let index = 0;
  images[index].classList.add("image-magnified");

  let intervalId = setInterval(function () {
    images[index].classList.remove("image-magnified");
    ++index;
    if (index >= images.length) {
      clearInterval(intervalId);
    } else {
      images[index].classList.add("image-magnified");
    }
  }, 1000);
}

function init() {
  loadImages();
}

init();
