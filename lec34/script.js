//
const wrapper = document.querySelector("#wrapper");
const ranger = document.querySelector("#ranger");
const cntViewer = document.querySelector("#cntViewer");
const starter = document.querySelector("#starter");

//
let startTime;
let endTime;
let flag = true;
let clickCard = [];
let doneCard = [];
let cardsCnt = 16;
let shuffledColors = [];
const colors = [
  "#a6f6f1",
  "#f08a5d",
  "#fca652",
  "#0f3057",
  "#00587a",
  "#de4463",
  "#006a71",
  "#3e978b",
  "#7d0633",
  "#f8bd7f",
  "#f6f6f6",
  "#81b214",
  "#8d93ab",
  "#cd0a0a",
  "#9cada4",
  "#382933",
  "#84a9ac",
  "#0f4c75",
  "#ff5722",
  "#f09ae9",
];

//
ranger.addEventListener("change", function () {
  cntViewer.textContent = this.value;
  cardsCnt = this.value;
});

starter.addEventListener("click", function () {
  init();
});

//
function makeCards() {
  for (let i = 0; i < shuffledColors.length; ++i) {
    //
    const card = document.createElement("div");
    card.className = "card";
    const cardInner = document.createElement("div");
    cardInner.className = "card-inner";
    const cardFront = document.createElement("div");
    cardFront.className = "card-front";
    const cardBack = document.createElement("div");

    //
    cardBack.className = "card-back";
    cardBack.style.backgroundColor = shuffledColors[i];

    //
    cardInner.appendChild(cardFront);
    cardInner.appendChild(cardBack);
    card.appendChild(cardInner);
    wrapper.appendChild(card);

    //
    card.addEventListener("click", function () {
      if (flag && !doneCard.includes(card)) {
        this.classList.toggle("flipped");
        clickCard.push(this);

        if (clickCard.length == 2) {
          if (
            clickCard[0].querySelector(".card-back").style.backgroundColor ==
            clickCard[1].querySelector(".card-back").style.backgroundColor
          ) {
            doneCard.push(clickCard[0]);
            doneCard.push(clickCard[1]);
            clickCard = [];

            if (doneCard.length == cardsCnt) {
              endTime = new Date();

              setTimeout(function () {
                alert("축하합니다. 기록은 : " + (endTime - startTime) / 1000 + "초 입니다.");
              }, 1000);
            }
          } else {
            flag = false;
            setTimeout(function () {
              clickCard[0].classList.remove("flipped");
              clickCard[1].classList.remove("flipped");
              flag = true;
              clickCard = [];
            }, 1000);
          }
        }
      }
    });
  }
}

function shuffleCards() {
  let newColors = [];
  let temp = [...colors];

  for (let i = 0; temp.length > 0; ++i) {
    newColors.push(temp.splice(Math.floor(Math.random() * temp.length), 1)[0]);
  }

  let cards = newColors.slice(0, cardsCnt / 2);

  for (let i = 0; i < cardsCnt / 2; ++i) cards.push(cards[i]);

  for (let i = 0; cards.length > 0; ++i) {
    shuffledColors.push(cards.splice(Math.floor(Math.random() * cards.length), 1)[0]);
  }
}

function flash() {
  document.querySelectorAll(".card").forEach(function (card, index) {
    setTimeout(function () {
      card.classList.add("flipped");
    }, 1000 + 100 * index);
  });

  setTimeout(function () {
    document.querySelectorAll(".card").forEach(function (card) {
      card.classList.remove("flipped");
    });
    flag = true;
    startTime = new Date();
  }, 5000);
}

function clear() {
  startTime = "";
  endTime = "";
  flag = false;
  wrapper.textContent = "";
  shuffledColors = [];
  doneCard = [];
}

function init() {
  clear();
  shuffleCards();
  makeCards();
  flash();
}
