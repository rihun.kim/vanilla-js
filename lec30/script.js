let arr = Array(45)
  .fill()
  .map((_, i) => i + 1);

let lotteryNum = [];
let bonusNum = 0;

// Shuffle WAY
function pickNum() {
  let pickArr = [];
  while (arr.length > 0) pickArr.push(arr.splice(Math.floor(Math.random() * arr.length), 1)[0]);

  lotteryNum = pickArr.slice(0, 6);
  bonusNum = pickArr[pickArr.length - 1];
}

function paint() {
  for (let i = 0; i < 6; ++i) {
    setTimeout(function () {
      console.log("setTimeout: ", i);
      const div = document.createElement("div");
      div.textContent = lotteryNum[i];
      div.style.width = "100px";
      div.style.height = "100px";
      div.style.borderRadius = "50%";
      div.style.backgroundColor = "red";
      div.style.lineHeight = "100px";
      div.style.textAlign = "center";
      document.body.appendChild(div);
    }, (i + 1) * 1000);
  }

  setTimeout(() => {
    const div = document.createElement("div");
    div.textContent = bonusNum;
    div.style.width = "100px";
    div.style.height = "100px";
    div.style.borderRadius = "50%";
    div.style.backgroundColor = "green";
    div.style.lineHeight = "100px";
    div.style.textAlign = "center";
    document.body.appendChild(div);
  }, 7000);
}

pickNum();
paint();
