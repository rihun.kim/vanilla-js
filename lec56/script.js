function createModal() {
  //
  const btns = document.querySelectorAll(".btn_delete");
  const deleteModal = document.querySelector(".cover_delete");
  const confirmModal = document.querySelector(".cover_confirm");
  //
  const deleteModalProceedBtn = document.querySelector("#btn_delete_proceed");
  const deleteModalStopBtn = document.querySelector("#btn_del_stop");
  const confirmModalProceedBtn = document.querySelector("#btn_confirm_proceed");

  //
  let delArticle;
  let scrollPos = 0;

  //
  function deleteArticle(e) {
    try {
      document.body.classList.add("noscroll");
      deleteModal.classList.remove("hidden");

      delArticle = e.target.parentNode;
    } catch (e) {
      console.err(e);
    }
  }
  //
  function proceedDelete(e) {
    try {
      delArticle.parentNode.removeChild(delArticle);

      confirmModal.classList.remove("hidden");
    } catch (e) {
      console.err(e);
    }
  }
  function stopDelete(e) {
    try {
      document.body.classList.remove("noscroll");
      deleteModal.classList.add("hidden");

      window.scrollTo(0, scrollPos);
    } catch (e) {
      console.err(e);
    }
  }
  function proceedConfirm(e) {
    try {
      document.body.classList.remove("noscroll");
      deleteModal.classList.add("hidden");
      confirmModal.classList.add("hidden");

      window.scrollTo(0, scrollPos);
    } catch (e) {
      console.err(e);
    }
  }

  //
  btns.forEach((btn) => btn.addEventListener("click", (e) => deleteArticle(e)));
  deleteModalProceedBtn.addEventListener("click", (e) => proceedDelete(e));
  deleteModalStopBtn.addEventListener("click", (e) => stopDelete(e));
  confirmModalProceedBtn.addEventListener("click", (e) => proceedConfirm(e));
  //
  window.addEventListener("scroll", () => {
    scrollPos = window.scrollY;
  });
}

function init() {
  createModal();
}

init();
