var module = (function () {
  //
  // form and input
  const form = document.querySelector("#form");
  const search = document.querySelector("#search");

  const result = document.querySelector("#result");
  const more = document.querySelector("#more");

  //
  const apiURL = "https://api.lyrics.ovh";

  //
  async function searchSongs(term) {
    // api 던지기
    const res = await fetch(`${apiURL}/suggest/${term}`);
    const data = await res.json();

    showData(data);
  }

  function showData(data) {
    // ul 과 li 는 스타일이 이미 지정되어 있음
    result.innerHTML = `
    <ul class="songs">
      ${data.data
        .map(
          (song) => `
      <li>
        <span><strong>${song.artist.name}</strong> - ${song.title}</span>
        <button class="btn" data-artist="${song.artist.name}" data-songtitle="${song.title}">Get Lyrics</button>
      </li>
      `
        )
        .join("")}
    </ul>
  `;

    if (data.prev || data.next) {
      more.innerHTML = `
      ${data.prev ? `<button class="btn" onclick="getMoreSongs('${data.prev}')">Prev</button>` : ""}
      ${data.next ? `<button class="btn" onclick="getMoreSongs('${data.next}')">Next</button>` : ""}
    `;
    } else {
      more.innerHTML = "";
    }
  }

  async function getMoreSongs(url) {
    const res = await fetch(`https://cors-anywhere.herokuapp.com/${url}`);
    const data = await res.json();

    showData(data);
  }

  async function getLyrics(artist, songTitle) {
    const res = await fetch(`${apiURL}/v1/${artist}/${songTitle}`);
    const data = await res.json();

    if (data.error) result.innerHTML = data.error;
    else {
      const lyrics = data.lyrics.replace(/(\r\n|\r|\n)/g, "<br>");

      // span 은 가사가 나오는 부분
      result.innerHTML = `
      <h2><strong>${artist}</strong> - ${songTitle}</h2><span>${lyrics}</span>
    `;
    }

    // 앞전에 있던 더보기 버튼 감추기
    more.innerHTML = "";
  }

  //
  form.addEventListener("submit", (e) => {
    e.preventDefault();

    const searchTerm = search.value.trim();

    if (!searchTerm) alert("Please Type in a search term");
    else searchSongs(searchTerm);
  });

  result.addEventListener("click", (e) => {
    const clickedEl = e.target;

    if (clickedEl.tagName == "BUTTON") {
      const artist = clickedEl.getAttribute("data-artist");
      const songTitle = clickedEl.getAttribute("data-songtitle");

      getLyrics(artist, songTitle);
    }
  });
})();
