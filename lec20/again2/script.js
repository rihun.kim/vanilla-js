//
const form = document.querySelector("#form");
const search = document.querySelector("#search");
const result = document.querySelector("#result");
const more = document.querySelector("#more");

//
const apiURL = "https://api.lyrics.ovh";

//
async function searchSongs(term) {
  const res = await fetch(`${apiURL}/suggest/${term}`);
  const data = await res.json();

  showData(data);
}

function showData(data) {
  console.log(data);
}

//
form.addEventListener("submit", (e) => {
  e.preventDefault();

  const searchTerm = search.value.trim();
  search.value = "";

  if(searchTerm !== "") {
    searchSongs(searchTerm)
  }
})
