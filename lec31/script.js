let type = "r";
const dic = {
  r: -1,
  s: 0,
  p: 1,
};
let interv;

function start() {
  interv = setInterval(() => {
    if (type == "r") type = "p";
    else if (type == "p") type = "s";
    else type = "r";

    document.querySelector("#result").textContent = type;
  }, 100);
}

document.querySelectorAll(".btn").forEach((btn) => {
  btn.addEventListener("click", function () {
    const res = dic[this.id] - dic[type];
    if (res == -1 || res == 2) document.querySelector("#res").textContent = "내가 이겼다!";
    else if (res == 0) document.querySelector("#res").textContent = "비겼다!";
    else document.querySelector("#res").textContent = "내가 졌다!";

    clearInterval(interv);
    setTimeout(start, 4000);
  });
});

start();
