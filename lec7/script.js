const video = document.querySelector("#video");
const play = document.querySelector("#play");
const stop = document.querySelector("#stop");
const progress = document.querySelector("#progress");
const timestamp = document.querySelector("#timestamp");

function toggleVideoStatus() {
  if (video.paused) video.play();
  else video.pause();
}

function updatePlayIcon() {
  if (video.paused) play.innerHTML = '<i class="fa fa-play fa-2x"></i>';
  else play.innerHTML = '<i class="fa fa-pause fa-2x"></i>';
}

function updateProgress() {
  progress.value = (video.currentTime / video.duration) * 100;

  let mins = Math.floor(video.currentTime / 60);
  if (mins < 10) mins = "0" + String(mins);

  let secs = Math.floor(video.currentTime % 60);
  if (secs < 10) secs = "0" + String(secs);

  timestamp.innerHTML = `${mins}:${secs}`;
}

function setVideoProgress() {
  video.currentTime = (+progress.value * video.duration) / 100;
}

function stopVideo() {
  video.currentTime = 0;
  video.pause();
}

video.addEventListener("timeupdate", updateProgress); // 비디오가 재생되면, 매순간순간마다 변화를 감지하는 리스너

video.addEventListener("click", toggleVideoStatus); // 비디오를 누르거나,
play.addEventListener("click", toggleVideoStatus); // 재생 버튼을 누르는 것은 동일한 행동

video.addEventListener("pause", updatePlayIcon);
video.addEventListener("play", updatePlayIcon);

stop.addEventListener("click", stopVideo);

progress.addEventListener("change", setVideoProgress); // range 영역을 누르면 불리는 리스너
