//
const player = document.querySelector(".player");
const video = player.querySelector(".viewer"); // video DOM
const progress = player.querySelector(".progress");
const progressBar = player.querySelector(".progress__filled");
const toggle = player.querySelector(".toggle");
// [] 는 해당 속성이 있는 것을 가져옴
// 뒤로감기, 앞으로감기 버튼들을 가져옴
const skipButtons = player.querySelectorAll("[data-skip]");

// 소리 슬라이더, 재생 속도 슬라이더
const ranges = player.querySelectorAll(".player__slider");

//
function togglePlay() {
  // 비디오 paused 로 상태 알 수 있음
  const method = video.paused ? "play" : "pause";
  video[method]();
}

function updateButton() {
  const icon = this.paused ? "►" : "❚ ❚";
  toggle.textContent = icon;
}

function skip() {
  // this 는 버튼을 의미
  // dataset 은 html 에서 data- 로 되어 있는 속성 값을 가져옴
  // 비디오 시간 조절
  video.currentTime += parseFloat(this.dataset.skip);
}

function handleRangeUpdate() {
  // 레인지에서 읽어 들인 값을
  // this 는 레인지이며, name 에 비디오 속성(volume, playbackRate) 값 존재
  video[this.name] = this.value;
}

function handleProgress() {
  // 퍼센트화 표기
  const percent = (video.currentTime / video.duration) * 100;
  // flexBasis 값이 증가하면서 노란색 길이가 길어짐
  // flexBasis 는 주어진 공간내에서 차지하는 크기를 의미
  progressBar.style.flexBasis = `${percent}%`;
}

function scrub(e) {
  // 비디오 재생 위치를 계산
  const scrubTime = (e.offsetX / progress.offsetWidth) * video.duration;
  video.currentTime = scrubTime;
}

//
// 비디오 자체를 누르면, 토글 플레이함
video.addEventListener("click", togglePlay);
// 재생 버튼에 달려 있는 토글 클래스
toggle.addEventListener("click", togglePlay);

// 비디오의 상태에 따른 감지 리스너
// 버튼 모양 변경
video.addEventListener("play", updateButton);
video.addEventListener("pause", updateButton);
// 프로그래스바 변경
video.addEventListener("timeupdate", handleProgress);

// 빨리감기, 뒤로감기 버튼들의 클릭 감지
skipButtons.forEach((button) => button.addEventListener("click", skip));

// 마우스의 무브나, 레인지의 변경을 감지
ranges.forEach((range) => range.addEventListener("change", handleRangeUpdate));
// 아래는 굳이 있을 필요없음
// ranges.forEach((range) => range.addEventListener("mousemove", handleRangeUpdate));

// 프로그래스바 리스너

// 이동시키는 스크럽
// 마우스 왼쪽만 감지 == click
progress.addEventListener("click", scrub);

// 아래 3개의 리스너는
// 드래그하면서 이동시키기 위한 기능
let mousedown = false;
progress.addEventListener("mousemove", (e) => mousedown && scrub(e));
// 마우스 왼쪽/오른쪽 상관 없이 리스닝
progress.addEventListener("mousedown", () => (mousedown = true));
progress.addEventListener("mouseup", () => (mousedown = false));
