const body = document.querySelector("body");

const IMAGE_NUM = 3;

function paintImage(imageNum) {
  const image = new Image();
  image.src = `./images/${imageNum + 1}.jpg`;
  image.classList.add("bgImage");
  body.appendChild(image);
}

function generateRandomNum() {
  return Math.floor(Math.random() * IMAGE_NUM);
}

function init() {
  const randomNum = generateRandomNum();
  paintImage(randomNum);
}

init();
