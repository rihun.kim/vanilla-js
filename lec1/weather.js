const COORDS = "coords";

function handleGeoSuccess(position) {
  console.log(position);
}

function handleGeoFailure(position) {
  console.error("ERROR", position);
}

function askForCoords() {
  navigator.geolocation.getCurrentPosition(handleGeoSuccess, handleGeoFailure);
}

function loadCoords() {
  const loadedCoords = localStorage.getItem(COORDS);
  if (loadedCoords == null) {
    askForCoords();
  } else {
  }
}

function init() {
  loadCoords();
}

init();
