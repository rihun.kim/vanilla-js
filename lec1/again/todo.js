//
const toDoForm = document.querySelector(".to-do-form");
const toDoInput = toDoForm.querySelector("input");
const toDoList = document.querySelector(".to-do-list");

//
const todos_ls = "todos";
let toDos = [];

//
function saveToDo() {
  localStorage.setItem(todos_ls, JSON.stringify(toDos));
}

function deleteToDo(e) {
  const btn = e.target;
  const li = btn.parentNode;

  // removeChild 는 엘리먼트를 보내야함
  toDoList.removeChild(li);

  const cleanToDos = toDos.filter(function (toDo) {
    return toDo.id != li.id;
  });

  toDos = cleanToDos;
  saveToDo();
}

function paintToDo(text) {
  const newId = toDos.length + 1;

  const li = document.createElement("li");
  const delBtn = document.createElement("button");
  const span = document.createElement("span");

  delBtn.textContent = "X";
  span.textContent = text;

  delBtn.addEventListener("click", deleteToDo);

  li.id = newId;
  li.appendChild(delBtn);
  li.appendChild(span);
  toDoList.appendChild(li);

  const toDoObj = {
    text: text,
    id: newId,
  };

  toDos.push(toDoObj);
  saveToDo();
}

function handleSubmit(event) {
  event.preventDefault();

  const currentValue = toDoInput.value;
  paintToDo(currentValue);

  toDoInput.value = "";
}

function loadToDos() {
  const loadToDos = localStorage.getItem(todos_ls);

  if (loadToDos !== null) {
    const parsedToDos = JSON.parse(loadToDos);
    parsedToDos.forEach(function (toDo) {
      paintToDo(toDo.text);
    });
  }
}

function init() {
  loadToDos();
  toDoForm.addEventListener("submit", handleSubmit);
}

init();
