//
const form = document.querySelector(".form");
const input = form.querySelector("input");
const greeting = document.querySelector(".greetings");

//
const user_ls = "currentUser";
const showing_cn = "showing";

//
function saveName(name) {
  localStorage.setItem(user_ls, name);
}

function handleSubmit(event) {
  event.preventDefault();

  const currentValue = input.value;
  paintGreeting(currentValue);
  saveName(currentValue);
}

function askForName() {
  form.classList.add(showing_cn);
  form.addEventListener("submit", handleSubmit);
}

function paintGreeting(name) {
  form.classList.remove(showing_cn);

  greeting.classList.add(showing_cn);
  greeting.textContent = `HELLO ${name}`;
}

// 최초 실행 내용
function loadName() {
  const currentUser = localStorage.getItem(user_ls);

  if (currentUser == null) askForName();
  else paintGreeting(currentUser);
}

function init() {
  loadName();
}

//
init();
