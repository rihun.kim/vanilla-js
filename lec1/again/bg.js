//
const body = document.querySelector("body");

//
const IMG_CNT = 3;

//
function paintImage(imageNum) {
  const image = new Image();

  image.src = `./images/${imageNum + 1}.jpg`;
  image.classList.add("bgImage");

  body.appendChild(image);
}

function generateRNum() {
  return Math.floor(Math.random() * IMG_CNT);
}

function init() {
  paintImage(generateRNum());
}

init();
