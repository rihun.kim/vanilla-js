//
const clockContainer = document.querySelector(".clock");
const clockTitle = document.querySelector("h1");

//
function getTime() {
  const date = new Date();
  const hours = date.getHours();
  const min = date.getMinutes();
  const sec = date.getSeconds();

  clockTitle.textContent = `${hours < 10 ? `0${hours}` : `${hours}`}:${min < 10 ? `0${min}` : `${min}`}:${
    sec < 10 ? `0${sec}` : `${sec}`
  }`;
}

function init() {
  setInterval(getTime, 1000);
}

init();
