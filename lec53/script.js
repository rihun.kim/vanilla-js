//
const video = document.querySelector(".player");
const canvas = document.querySelector(".photo");
const ctx = canvas.getContext("2d");
const strip = document.querySelector(".strip");
const snap = document.querySelector(".snap");

//
function getVideo() {
  // 카메라는 네비게이터 에 존재
  navigator.mediaDevices
    .getUserMedia({ video: true, audio: false })
    .then((localMediaStream) => {
      // localMediaStream 이 카메라를 의미
      console.log(localMediaStream);

      // 카메라의 내용을 비디오에 연결 시켜 재생시킴
      // 오른쪽 상단의 조그만 동영상 부분을 의미
      video.srcObject = localMediaStream;
      video.play();
    })
    .catch((err) => {
      console.err(`OH NO!!`, err);
    });
}

function paintToCanvas() {
  // 캔버스가 중앙에 있는 큰 화면을 의미

  const width = video.videoWidth;
  const height = video.videoHeight;
  canvas.width = width;
  canvas.height = height;

  // 지속적으로 캔버스에 비디오에서 가져온 이미지를 그려줌
  return setInterval(() => {
    ctx.drawImage(video, 0, 0, width, height);
  }, 16);
}

function takePhoto() {
  snap.currentTime = 0;
  snap.play();

  const data = canvas.toDataURL("image/jpeg");
  const link = document.createElement("a");
  link.href = data;
  link.setAttribute("download", "handsome");
  link.innerHTML = `<img src="${data}" alt="Handsome Man" />`;
  strip.insertBefore(link, strip.firstChild);
}

//
getVideo();
video.addEventListener("canplay", paintToCanvas);
