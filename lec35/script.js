//
const turnButton = document.querySelector("#turn-btn");

//
const rival = {
  hero: document.querySelector("#rival-hero"),
  deck: document.querySelector("#rival-deck"),
  field: document.querySelector("#rival-cards"),
  cost: document.querySelector("#rival-cost"),
  deckData: [],
  heroData: [],
  fieldData: [],
  choiceCard: null,
  choiceCardData: null,
};

const my = {
  hero: document.querySelector("#my-hero"),
  deck: document.querySelector("#my-deck"),
  field: document.querySelector("#my-cards"),
  cost: document.querySelector("#my-cost"),
  deckData: [],
  heroData: [],
  fieldData: [],
  choiceCard: null,
  choiceCardData: null,
};

let turn = true; // true: my, false: rival

//
function deckToField(data, myTurn) {
  const obj = myTurn ? my : rival;
  let nowCost = +obj.cost.textContent;

  if (nowCost < data.cost) return "end";

  const idx = obj.deckData.indexOf(data);
  obj.deckData.splice(idx, 1);
  obj.fieldData.push(data);

  repaintField(obj);
  repaintDeck(obj);
  data.field = true;
  obj.cost.textContent = nowCost - data.cost;
}

function repaintField(obj) {
  obj.field.innerHTML = "";
  obj.fieldData.forEach(function (data) {
    connectCardDOM(data, obj.field);
  });
}

function repaintDeck(obj) {
  obj.deck.innerHTML = "";
  obj.deckData.forEach(function (data) {
    connectCardDOM(data, obj.deck);
  });
}

function repaintHero(obj) {
  obj.hero.innerHTML = "";
  connectCardDOM(obj.heroData, obj.hero, true);
}

function repaintScreen(myScreen) {
  const obj = myScreen ? my : rival;

  repaintField(obj);
  repaintDeck(obj);
  repaintHero(obj);
}

function turnAction(card, data, myTurn) {
  const ally = myTurn ? my : rival;
  const enemy = myTurn ? rival : my;

  if (card.classList.contains("card-turnover")) return;

  const enemyCard = myTurn ? !data.mine : data.mine;
  if (enemyCard && ally.choiceCard) {
    data.hp = data.hp - ally.choiceCardData.att;
    if (data.hp <= 0) {
      const idx = enemy.fieldData.indexOf(data);
      if (idx > -1) {
        enemy.fieldData.splice(idx, 1);
      } else {
        alert("Victory!!!");
        init();
      }
    }

    repaintScreen(!myTurn);
    ally.choiceCard.classList.remove("card-selected");
    ally.choiceCard.classList.add("card-turnover");
    ally.choiceCard = null;
    ally.choiceCardData = null;
  } else if (enemyCard) {
    return;
  }

  if (data.field) {
    document.querySelectorAll(".card").forEach(function (card) {
      card.classList.remove("card-selected");
    });
    card.classList.add("card-selected");
    ally.choiceCard = card;
    ally.choiceCardData = data;
  } else {
    if (deckToField(data, myTurn) !== "end") {
      myTurn ? createMyDeck(1) : createEnemyDeck(1);
    }
  }
}

function connectCardDOM(data, dom, hero) {
  const card = document.querySelector(".card-hidden .card").cloneNode(true);
  card.querySelector(".card-cost").textContent = data.cost;
  card.querySelector(".card-att").textContent = data.att;
  card.querySelector(".card-hp").textContent = data.hp;

  if (hero) {
    card.querySelector(".card-cost").style.display = "none";
    const name = document.createElement("div");
    name.textContent = "HERO";
    card.appendChild(name);
  }

  card.addEventListener("click", function () {
    turnAction(card, data, turn);
  });

  dom.appendChild(card);
}

function createEnemyDeck(num) {
  for (let i = 0; i < num; ++i) rival.deckData.push(cardFactory());

  repaintDeck(rival);
}

function createMyDeck(num) {
  for (let i = 0; i < num; ++i) my.deckData.push(cardFactory(false, true));

  repaintDeck(my);
}

function createMyHero() {
  my.heroData = cardFactory(true, true);
  connectCardDOM(my.heroData, my.hero, true);
}

function createRivalHero() {
  rival.heroData = cardFactory(true);
  connectCardDOM(rival.heroData, rival.hero, true);
}

//
function Card(hero, myCard) {
  if (hero) {
    this.att = Math.ceil(Math.random() * 2);
    this.hp = Math.ceil(Math.random() * 5) + 25;
    this.hero = true;
    this.field = true;
  } else {
    this.att = Math.ceil(Math.random() * 5);
    this.hp = Math.ceil(Math.random() * 5);
    this.cost = Math.floor((this.att + this.hp) / 2);
  }

  if (myCard) this.mine = true;
}

function cardFactory(hero, myCard) {
  return new Card(hero, myCard);
}

function init() {
  [rival, my].forEach(function (item) {
    item.deckData = [];
    item.heroData = [];
    item.fieldData = [];
    item.choiceCard = [];
    item.choiceCardData = [];
  });

  createEnemyDeck(5);
  createMyDeck(5);
  createMyHero();
  createRivalHero();
  repaintScreen(true);
  repaintScreen(false);
}

turnButton.addEventListener("click", function () {
  const obj = turn ? my : rival;

  document.querySelector("#rival").classList.toggle("turn");
  document.querySelector("#my").classList.toggle("turn");

  repaintField(obj);
  repaintHero(obj);

  turn = !turn;
  if (turn) my.cost.textContent = 10;
  else rival.cost.textContent = 10;
});

init();
