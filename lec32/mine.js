//
const execButton = document.querySelector("#exec");
const hor = document.querySelector("#hor");
const ver = document.querySelector("#ver");
const mine = document.querySelector("#mine");
const tbody = document.querySelector("#table tbody");
const panel = document.querySelector("#panel");
const timer = document.querySelector("#time");

//
let mineSet = [];
let flagSet = [];
let dataSet = [];
let timeInterver;

//
function randomMine() {
  const size = +hor.value * +ver.value;

  let arr = Array(size)
    .fill()
    .map((_, i) => i);

  let newArr = [];
  for (let i = 0; i < size; ++i) newArr.push(arr.splice(Math.floor(Math.random() * arr.length), 1)[0]);

  return newArr.slice(0, +mine.value);
}

function startTime() {
  let time = 0;
  timeInterver = setInterval(function () {
    timer.innerText = ++time;
  }, 1000);
}

//
execButton.addEventListener("click", function () {
  if (timer != undefined) {
    clearInterval(timeInterver);
    startTime();
  }

  tbody.innerHTML = "";
  const horSize = +hor.value;
  const verSize = +ver.value;

  for (let row = 0; row < horSize; ++row) {
    dataSet.push([]);
    const tr = document.createElement("tr");

    for (let col = 0; col < verSize; ++col) {
      dataSet[row][col] = 1;
      const td = document.createElement("td");

      td.addEventListener("click", function (e) {
        td.classList.add("opend");

        if (dataSet[row][col] == "X") {
          td.classList.add("mine");
          td.textContent = "X";
          panel.innerHTML = "YOU LOOOOSE :P<br/>Click execute button for NEW game";
          // setTimeout(function () {
          //   window.location.reload();
          // }, 10000);
        } else {
          const x = [-1, 1, -1, 1, -1, 1, 0, 0];
          const y = [-1, -1, 1, 1, 0, 0, -1, 1];

          let minesCnt = 0;
          let boxes = [];
          for (let i = 0; i < 9; ++i) {
            if (row + x[i] >= 0 && row + x[i] < horSize && col + y[i] >= 0 && col + y[i] < verSize) {
              if (dataSet[row + x[i]][col + y[i]] == "X") ++minesCnt;
              else boxes.push([row + x[i], col + y[i]]);
            }
          }
          td.textContent = minesCnt;

          if (minesCnt == 0) {
            for (let box of boxes) {
              if (!tbody.children[box[0]].children[box[1]].classList.contains("opend")) {
                tbody.children[box[0]].children[box[1]].click();
              }
            }
          }
        }
      });

      td.addEventListener("contextmenu", function (e) {
        e.preventDefault();
        const type = td.textContent;

        if (type == "" && flagSet.length < mineSet.length) {
          td.textContent = "!";

          const res = findRowCol(td);
          addFlagSet(res[0], res[1]);
          if (flagSet.length == mineSet.length) {
            if (matchAnswer()) {
              panel.innerHTML = "YOU WIN!!! GO TO LAS VEGAS!!<br/>Click execute button for NEW game";
            }
          }
        } else if (type == "!") {
          td.textContent = "?";
          const res = findRowCol(td);
          deleteFlagSet(res[0], res[1]);
        } else if (type == "?") td.textContent = "";
      });

      tr.appendChild(td);
    }

    tbody.appendChild(tr);
  }

  const randomMines = randomMine();

  for (let i = 0; i < randomMines.length; ++i) {
    let row = parseInt(randomMines[i] / horSize);
    let col = randomMines[i] % verSize;
    // tbody.children[row].children[col].textContent = "X";
    dataSet[row][col] = "X";
    mineSet.push([row, col]);
  }
});

function findRowCol(td) {
  for (let i = 0; i < +hor.value; ++i) {
    if (tbody.children[i].contains(td)) {
      for (let j = 0; j < +ver.value; ++j) {
        if (tbody.children[i].children[j] == td) {
          return [i, j];
        }
      }
    }
  }
}

function matchAnswer() {
  flagSet.sort((a, b) => a[0] - b[0]);
  mineSet.sort((a, b) => a[0] - b[0]);

  const len = flagSet.length;
  for (let i = 0; i < len; ++i) {
    if (flagSet[i][0] != mineSet[i][0] || flagSet[i][1] != mineSet[i][1]) {
      return false;
    }
  }

  return true;
}

function addFlagSet(row, col) {
  flagSet.push([row, col]);
}

function deleteFlagSet(row, col) {
  for (let i = 0; i < flagSet.length; ++i) {
    if (flagSet[i][0] == row && flagSet[i][1] == col) {
      flagSet.splice(i, 1);
      return;
    }
  }
}

//
panel.innerHTML =
  "! = flag => first time Right Click<br/>? = Maybe Mine => second time Right Click<br/>X = Mine<br/>MUST flag number <= mine number";
