//
const word = document.querySelector("#word");
const text = document.querySelector("#text");
const scoreEl = document.querySelector("#score");
const timeEl = document.querySelector("#time");
const endgameEl = document.querySelector("#end-game-container");
const settingsBtn = document.querySelector("#settings-btn");
const settings = document.querySelector("#settings");
const settingsForm = document.querySelector("#settings-form");
const difficultySelect = document.querySelector("#difficulty");

//
const words = [];
let randomWord;
let score = 0;
let time = 15;
let difficulty = localStorage.getItem("difficulty") != null ? localStorage.getItem("difficulty") : "medium";
difficultySelect.value = localStorage.getItem("difficulty") !== null ? localStorage.getItem("difficulty") : "medium";
let timeInterval;

//
function getRandomWord() {
  return words[Math.floor(Math.random() * words.length)];
}

async function setRandomWord() {
  await fetch("https://random-word-api.herokuapp.com/word?number=50")
    .then((res) => res.json())
    .then((_words) => _words.forEach((word) => words.push(word)));
}

function addWordToDOM() {
  randomWord = getRandomWord();
  word.innerHTML = randomWord;
}

function updateScore() {
  score++;
  scoreEl.innerHTML = score;
}

function updateTime() {
  time--;
  timeEl.innerHTML = time + "s";

  if (time == 0) {
    clearInterval(timeInterval);
    gameOver();
  }
}

function gameOver() {
  endgameEl.innerHTML = `
  <h1>Time ran out</h1>
  <p>Your final Score is ${score}</p>
  <button onclick="location.reload()">Reload</button>`;

  endgameEl.style.display = "flex";
}

async function init() {
  text.focus();
  await setRandomWord();
  timeInterval = setInterval(updateTime, 1000);
  addWordToDOM();
}

init();

//
text.addEventListener("input", (e) => {
  const insertedText = e.target.value;

  if (insertedText == randomWord) {
    addWordToDOM();
    updateScore();

    e.target.value = "";

    if (difficulty == "hard") time += 2;
    else if (difficulty == "medium") time += 3;
    else time += 5;

    updateTime();
  }
});

settingsBtn.addEventListener("click", () => settings.classList.toggle("hide"));

settingsForm.addEventListener("change", (e) => {
  difficulty = e.target.value;
  localStorage.setItem("difficulty", difficulty);
});
