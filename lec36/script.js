//
const table = document.querySelector("#table");
const score = document.querySelector("#score");

//
let data = [];
let dragStart = false;
let draging = false;
let startPoint;
let endPoint;

//
init();
createRandom();
paint();

//
function init() {
  //
  const fragment = document.createDocumentFragment();

  //
  [1, 2, 3, 4].forEach(function () {
    data.push([0, 0, 0, 0]);
    let tr = document.createElement("tr");

    //
    [1, 2, 3, 4].forEach(function () {
      const td = document.createElement("td");
      tr.appendChild(td);
    });

    fragment.appendChild(tr);
  });

  table.appendChild(fragment);
}

function createRandom() {
  let temp = [];

  //
  data.forEach(function (rowData, i) {
    rowData.forEach(function (colData, j) {
      // 빈 공간만 골라내기
      // 기본적으로 colData = 0;
      if (!colData) temp.push([i, j]);
    });
  });

  //
  if (temp.length === 0) {
    alert("게임오버: " + score.textContent);
    table.innerHTML = "";
    init();
  } else {
    // 임의의 위치에 2 넣기
    var randomBox = temp[Math.floor(Math.random() * temp.length)];
    data[randomBox[0]][randomBox[1]] = 2;
    paint();
  }
}

function paint() {
  data.forEach(function (rowData, i) {
    rowData.forEach(function (colData, j) {
      if (colData > 0) table.children[i].children[j].textContent = colData;
      else table.children[i].children[j].textContent = "";
    });
  });
}

//
window.addEventListener("mousedown", function (event) {
  dragStart = true;
  // clientX 브라우저 상의 좌표
  startPoint = [event.clientX, event.clientY];
});

window.addEventListener("mousemove", function (event) {
  if (dragStart) draging = true;
});

window.addEventListener("mouseup", function (event) {
  endPoint = [event.clientX, event.clientY];

  let way;
  if (draging) {
    let diffX = endPoint[0] - startPoint[0];
    let diffY = endPoint[1] - startPoint[1];

    if (diffX < 0 && Math.abs(diffX) / Math.abs(diffY) > 1) way = "left";
    else if (diffX > 0 && Math.abs(diffX) / Math.abs(diffY) > 1) way = "right";
    else if (diffY > 0 && Math.abs(diffX) / Math.abs(diffY) < 1) way = "down";
    else if (diffY < 0 && Math.abs(diffX) / Math.abs(diffY) < 1) way = "up";
  }

  dragStart = false;
  draging = false;

  let newData = [[], [], [], []];
  switch (way) {
    case "left":
      data.forEach(function (rowData, i) {
        rowData.forEach(function (colData, j) {
          if (colData) {
            // colData 값이 존재할 경우 들어옴
            // 빈칸들은 무시되게 됨

            //
            if (newData[i][newData[i].length - 1] && newData[i][newData[i].length - 1] === colData) {
              newData[i][newData[i].length - 1] *= 2;
              score.textContent = +score.textContent + newData[i][newData[i].length - 1];
            } else {
              newData[i].push(colData);
            }
          }
        });
      });

      // refreshing
      [1, 2, 3, 4].forEach(function (rowData, i) {
        [1, 2, 3, 4].forEach(function (colData, j) {
          data[i][j] = newData[i][j] || 0;
        });
      });

      break;
    case "right":
      // 연산만 처리
      data.forEach(function (rowData, i) {
        rowData.forEach(function (colData, j) {
          if (colData) {
            if (newData[i][0] && newData[i][0] === colData) {
              newData[i][0] *= 2;
              score.textContent = +score.textContent + newData[i][0];
            } else newData[i].unshift(colData);
          }
        });
      });

      // refreshing
      // 실제 움직이기
      [1, 2, 3, 4].forEach(function (rowData, i) {
        [1, 2, 3, 4].forEach(function (colData, j) {
          data[i][3 - j] = newData[i][j] || 0;
        });
      });

      break;
    case "up":
      data.forEach(function (rowData, i) {
        rowData.forEach(function (colData, j) {
          if (colData) {
            if (newData[j][newData[j].length - 1] && newData[j][newData[j].length - 1] === colData) {
              newData[j][newData[j].length - 1] *= 2;
              score.textContent = +score.textContent + newData[i][0] + newData[j][newData[j].length - 1];
            } else newData[j].push(colData);
          }
        });
      });

      // refreshing
      [1, 2, 3, 4].forEach(function (colData, i) {
        [1, 2, 3, 4].forEach(function (rowData, j) {
          data[j][i] = newData[i][j] || 0;
        });
      });

      break;
    case "down":
      data.forEach(function (rowData, i) {
        rowData.forEach(function (colData, j) {
          if (colData) {
            if (newData[j][0] && newData[j][0] === colData) {
              newData[j][0] *= 2;
              score.textContent = +score.textContent + newData[i][0] + newData[j][0];
            } else newData[j].unshift(colData);
          }
        });
      });

      // refreshing
      [1, 2, 3, 4].forEach(function (colData, i) {
        [1, 2, 3, 4].forEach(function (rowData, j) {
          data[3 - j][i] = newData[i][j] || 0;
        });
      });

      break;
  }

  paint();
  createRandom();
});
