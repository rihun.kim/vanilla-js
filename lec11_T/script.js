const module = (function () {
  function createGraph(datas) {
    const body = document.querySelector("body");

    const colors = ["red", "orange", "yellow", "green", "blue"];

    for (let data of datas) {
      let sumData = 0;
      for (let idx in data) sumData += data[idx];

      const wrapper = document.createElement("div");
      wrapper.style.width = "1000px";
      wrapper.style.height = "50px";
      wrapper.style.display = "flex";
      wrapper.style.marginBottom = "10px";

      const tenPortion = sumData * 0.1;
      let colorIdx = 0;
      for (let idx in data) {
        let portion = ((data[idx] / sumData) * 100).toFixed(2);

        const div = document.createElement("div");

        div.style.height = "50px";
        div.style.backgroundColor = colors[colorIdx++];
        div.innerText = idx + " " + portion + "%";

        if (data[idx] < tenPortion) div.style.minWidth = "10%";
        else div.style.width = portion + "%";

        wrapper.appendChild(div);
      }

      body.appendChild(wrapper);
    }
  }

  return {
    createGraph: createGraph,
  };
})();

const datas = [
  {
    apple: 452562,
    samsung: 235345,
    LG: 33414,
    nokia: 11432,
  },
  // {
  //   chrome: 347372,
  //   firefox: 342214,
  //   ie: 124553,
  //   opera: 34134,
  //   whale: 12342,
  // },
];
module.createGraph(datas);
