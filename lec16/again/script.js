// 무한 스크롤, infinite scroll

//
const filter = document.querySelector("#filter");
const postsContainer = document.querySelector("#posts-container");
const loading = document.querySelector(".loader");

//
let limit = 5;
let page = 1;

//
async function getPosts() {
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts?_limit=${limit}&_page=${page}`);

  const data = await res.json();
  return data;
}

async function showPosts() {
  const posts = await getPosts();

  posts.forEach((post) => {
    const postEl = document.createElement("div");

    postEl.classList.add("post");
    postEl.innerHTML = `
      <div class="number">${post.id}</div>
      <div class="post-info">
        <h2 class="post-title">${post.title}</h2>
        <p class="post-body">${post.body}</p>
      </div>
    `;

    postsContainer.appendChild(postEl);
  });
}

function showLoading() {
  // setTimeout 때문에 1초 정도 지속
  loading.classList.add("show");

  setTimeout(() => {
    loading.classList.remove("show");
    setTimeout(() => {
      page++;
      showPosts(); // More resource getting
    }, 300);
  }, 1000);
}

function filterPosts(e) {
  const term = e.target.value.toUpperCase();
  const posts = document.querySelectorAll(".post");

  posts.forEach((post) => {
    // 태그안에 있는 텍스트 가져오기
    const title = post.querySelector(".post-title").textContent.toUpperCase();
    const body = post.querySelector(".post-body").textContent.toUpperCase();

    if (title.indexOf(term) > -1 || body.indexOf(term) > -1) post.style.display = "flex";
    else post.style.display = "none";
  });
}

//
filter.addEventListener("input", filterPosts);

window.addEventListener("scroll", () => {
  const { scrollTop, scrollHeight, clientHeight } = document.documentElement;

  // clientHeight : 화면의 높이 크기
  // scrollTop : 스크롤 하는 부분의 맨 위 상단 좌표
  // scrollHeight : 스크롤을 이끄는 엘리먼트의 높이 크기
  // console.log(scrollTop, scrollHeight, clientHeight);

  // 엘리먼트의 끝과 비슷해졌는지를 파악
  if (scrollTop + clientHeight >= scrollHeight) {
    showLoading();
  }
});

//
showPosts();
