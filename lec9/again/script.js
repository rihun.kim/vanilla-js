//
const main = document.querySelector("#main");
const addUserBtn = document.querySelector("#add");
const doubleBtn = document.querySelector("#double");
const showBtn = document.querySelector("#show");
const sortBtn = document.querySelector("#sort");
const calculateBtn = document.querySelector("#calculate");

//
let data = [];

//
async function getRandomUser() {
  const res = await fetch("https://randomuser.me/api");
  const data = await res.json();
  const user = data.results[0];

  const newUser = {
    name: `${user.name.first} ${user.name.last}`,
    money: Math.floor(Math.random() * 1000000),
  };

  // 배열에 객체 넣기
  addData(newUser);
}

function addData(obj) {
  data.push(obj);

  updateDOM();
}

function updateDOM(providedData = data) {
  // 맨 앞에 오는 내용, h2 에 밑줄 존재
  main.innerHTML = "<h2><strong>Person</strong> Wealth</h2>";

  providedData.forEach((item) => {
    const element = document.createElement("div");
    element.classList.add("person");
    element.innerHTML = `<strong>${item.name}</strong>${formatMoney(item.money)}`;

    main.appendChild(element);
  });
}

// 구조적으로 데이터와 렌더링을 구분하여 제작한 설계 굿
function doubleMoney() {
  data = data.map((user) => {
    return { ...user, money: user.money * 2 };
  });

  updateDOM();
}

function sortByRichest() {
  data.sort((a, b) => b.money - a.money);

  updateDOM();
}

function show() {
  data = data.filter((user) => user.money > 1000000);

  updateDOM();
}

function calculate() {
  const wealth = data.reduce((acc, user) => (acc += user.money), 0);

  const wealthEl = document.createElement("div");
  wealthEl.innerHTML = `<h3>Total Wealth: <strong>${formatMoney(wealth)}</strong></h3>`;

  main.appendChild(wealthEl);
}

function formatMoney(number) {
  return "$" + number.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
}

//
addUserBtn.addEventListener("click", getRandomUser);
doubleBtn.addEventListener("click", doubleMoney);
sortBtn.addEventListener("click", sortByRichest);
showBtn.addEventListener("click", show);
calculateBtn.addEventListener("click", calculate);

//
getRandomUser();
getRandomUser();
getRandomUser();
