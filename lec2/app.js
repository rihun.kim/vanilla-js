const INITIAL_COLOR = "#2c2c2c";
const CANVAS_SIZE = 500;

const canvas = document.querySelector("canvas");
canvas.width = CANVAS_SIZE;
canvas.height = CANVAS_SIZE;

const ctx = canvas.getContext("2d");
ctx.lineWidth = 2.5;
ctx.strokeStyle = INITIAL_COLOR;
ctx.fillStyle = "white";
ctx.fillRect(0, 0, CANVAS_SIZE, CANVAS_SIZE);

const colors = document.querySelectorAll(".jsColor");
const range = document.querySelector("#jsRange");
const mode = document.querySelector("#jsMode");
const saveBtn = document.querySelector("#jsSave");

let painting = false;
let filling = false;

function startPainting() {
  painting = true;
}

function stopPainting() {
  painting = false;
}

function handleMouseMove(event) {
  const x = event.offsetX;
  const y = event.offsetY;

  if (!painting) {
    ctx.beginPath();
    ctx.moveTo(x, y);
  } else {
    ctx.lineTo(x, y);
    ctx.stroke();
  }
}

function handleClickColor(event) {
  const color = event.target.style.backgroundColor;
  ctx.strokeStyle = color;
  ctx.fillStyle = color;
}

function handleClickRange(event) {
  const lineWidth = event.target.value;
  ctx.lineWidth = lineWidth;
}

function handleClickMode() {
  const modeTag = mode.innerText;
  if (modeTag == "DRAW") {
    mode.innerText = "FILL";
    filling = true;
  } else {
    mode.innerText = "DRAW";
    filling = false;
  }
}

function handleClickCanvas() {
  if (filling) {
    ctx.fillRect(0, 0, CANVAS_SIZE, CANVAS_SIZE);
  }
}

function handleClickSave() {
  const imageUrl = canvas.toDataURL();
  const link = document.createElement("a");
  const date = new Date();
  link.href = imageUrl;
  link.download = "PaintJS[🎨] " + date.toDateString();
  link.click();
}

function handleClickCM(event) {
  event.preventDefault();
}

function init() {
  if (canvas) {
    canvas.addEventListener("mousemove", handleMouseMove);
    canvas.addEventListener("mousedown", startPainting);
    canvas.addEventListener("mouseup", stopPainting);
    canvas.addEventListener("mouseleave", stopPainting);
    canvas.addEventListener("click", handleClickCanvas);
    canvas.addEventListener("contextmenu", handleClickCM);
  }

  colors.forEach((color) => color.addEventListener("click", handleClickColor));
  range.addEventListener("click", handleClickRange);
  mode.addEventListener("click", handleClickMode);
  saveBtn.addEventListener("click", handleClickSave);
}

init();
