function init() {
  const cnt = document.querySelector("#count");
  const plusBtn = document.querySelector("#plusBtn");
  const minusBtn = document.querySelector("#minusBtn");

  let count = 0;
  cnt.innerText = count;

  function add() {
    cnt.innerText = ++count;
  }
  function subtract() {
    cnt.innerText = --count;
  }

  //
  let timerId;
  let intervalId;
  plusBtn.addEventListener("mousedown", function () {
    add();
    intervalId = setTimeout(function () {
      intervalId = setInterval(add, 70);
    }, 500);
  });
  plusBtn.addEventListener("mouseup", function () {
    clearInterval(intervalId);
    clearTimeout(timerId);
  });

  minusBtn.addEventListener("mousedown", function () {
    subtract();
    intervalId = setTimeout(function () {
      intervalId = setInterval(subtract, 70);
    }, 500);
  });
  minusBtn.addEventListener("mouseup", function () {
    clearInterval(intervalId);
    clearTimeout(timerId);
  });
}

init();
