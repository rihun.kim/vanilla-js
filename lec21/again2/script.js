var module = (function () {
  function start() {
    //
    const container = document.querySelector("#container");
    const text = document.querySelector("#text");

    //
    const totalTime = 7500;
    const breathTime = (totalTime / 5) * 2;
    const holdTime = totalTime / 5;

    function breathAnimation() {
      text.textContent = "Breath In!";
      container.className = "container grow";

      setTimeout(() => {
        text.textContent = "HOLD";
        setTimeout(() => {
          text.textContent = "Breath OUT!";
          container.className = "container shrink";
        }, holdTime);
      }, breathTime);
    }

    breathAnimation();
    setInterval(breathAnimation, totalTime);
  }

  return {
    start: start,
  };
})();

module.start();
