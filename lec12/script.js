const wordEl = document.querySelector("#word");
const wrongLettersEl = document.querySelector("#wrong-letters");

const playAgainBtn = document.querySelector("#play-button");
const popup = document.querySelector("#popup-container");

const notification = document.querySelector("#notification-container");

const finalMessage = document.querySelector("#final-message");
const finalMessageRevealWord = document.querySelector("#final-message-reveal-word");

const figureParts = document.querySelectorAll(".figure-part");

const words = [];

let selectedWord = "";
let playable = true;

const correctLetters = [];
const wrongLetters = [];

async function getRandomWord() {
  await fetch("https://random-word-api.herokuapp.com/word?number=30")
    .then((res) => res.json())
    .then((_words) => _words.forEach((word) => words.push(word)));

  selectedWord = words[Math.floor(Math.random() * words.length)];
}

function displayWord() {
  wordEl.innerHTML = `${selectedWord
    .split("")
    .map((letter) => `<span class="letter">${correctLetters.includes(letter) ? letter : ""}</span>`)
    .join("")}`;

  const innerWord = wordEl.innerText.replace(/[ \n]/g, "");

  if (innerWord === selectedWord) {
    finalMessage.innerText = "Congratulations! You Won!";
    popup.style.display = "flex";

    playable = false;
  }
}

function updateWrongLettersEl() {
  wrongLettersEl.innerHTML = `
  ${wrongLetters.length > 0 ? "<p>Wrong</p>" : ""}
  ${wrongLetters.map((letter) => `<span>${letter}</span>`)}
  `;

  figureParts.forEach((part, index) => {
    const errors = wrongLetters.length;

    if (index < errors) part.style.display = "block";
    else part.style.display = "none";
  });

  if (wrongLetters.length === figureParts.length) {
    finalMessage.innerText = "Unfortunately you lost.";
    finalMessageRevealWord.innerText = `...the word was: ${selectedWord}`;
    popup.style.display = "flex";

    playable = false;
  }
}

function showNotification() {
  notification.classList.add("show");

  setTimeout(() => {
    notification.classList.remove("show");
  }, 2000);
}

window.addEventListener("keydown", (e) => {
  if (playable) {
    if (e.keyCode >= 65 && e.keyCode <= 90) {
      const letter = e.key.toLowerCase();

      if (selectedWord.includes(letter)) {
        if (!correctLetters.includes(letter)) {
          correctLetters.push(letter);

          displayWord();
        } else {
          showNotification();
        }
      } else {
        if (!wrongLetters.includes(letter)) {
          wrongLetters.push(letter);
          updateWrongLettersEl();
        } else {
          showNotification();
        }
      }
    }
  }
});

playAgainBtn.addEventListener("click", () => {
  playable = true;

  correctLetters.splice(0);
  wrongLetters.splice(0);

  selectedWord = words[Math.floor(Math.random() * words.length)];

  displayWord();
  updateWrongLettersEl();

  popup.style.display = "none";
});

async function init() {
  await getRandomWord();
  displayWord();
}

init();
